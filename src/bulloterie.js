import * as Matter from 'matter-js'

import User from "/src/user.js"
import Legend from "/src/legend.js"
import Bulle from "/src/bulle.js"

export default class Bulloterie {
    constructor(params) {
        this.name = params.name
        this.bulles = []
        this.users = []
        this.legends = []
    }

    import(data) {
        data.users.forEach((user_data) => {
            this.users.push(new User(user_data))
        })

        data.legends.forEach((legend_data) => {
            this.legends.push(new Legend(legend_data))
        })

        data.bulles.forEach((bulle_data) => {
            bulle_data.bulloterie = this
            this.bulles.push(new Bulle(bulle_data))
        })
    }

    display() {

        // create an engine
        let engine = Matter.Engine.create();

        // create a renderer
        let render = Matter.Render.create({
            element: document.body,
            engine: engine,
            options: {
                wireframes: false,
                background: "red",
            
            }
        });

        let bodies = []

        console.log(`Bulloterie: ${this.name}`)
        console.log("")

        this.users.forEach((user) => {
            user.display()
        })
        console.log("")

        this.legends.forEach((legend) => {
            legend.display()
        })
        console.log("")

        this.bulles.forEach((bulle) => {
            bodies = bodies.concat(bulle.display())
        })

        console.log(bodies)
        // add all of the bodies to the world
        Matter.World.add(engine.world, bodies);

        engine.world.gravity.x = 0
        engine.world.gravity.y = 0

        // run the engine
        Matter.Engine.run(engine);

        // run the renderer
        Matter.Render.run(render);
    }

    addBulle(params) {
        params.bulloterie = this
        let bulle = new Bulle(params)

        this.bulles.push(bulle)
    }

    addUser(params) {
        let user = new User(params)

        this.users.push(user)
    }

    addLegend(params) {
        let legend = new Legend(params)

        this.legends.push(legend)
    }
}