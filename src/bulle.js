import * as Matter from 'matter-js'

import Symbol from "/src/symbol.js"

export default class Bulle {
    constructor(params) {
        this.name = params.name
        this.bulloterie = params.bulloterie
        this.symbols = []

        if (params.symbols) {
            params.symbols.forEach((symbol) => {
                this.importSymbol(symbol)
            })
        }
    }

    importSymbol(symbol_data) {
        let user = this.bulloterie.users.find((user) => {
            return user.name == symbol_data.user
        })

        let legend = this.bulloterie.legends.find((legend) => {
            return legend.name == symbol_data.legend
        })

        this.addSymbol({ "user": user, "legend": legend })
    }

    diameter(){
        return this.symbols.length*10
    }

    display() {
        console.log(`Bulle: ${this.name}`)

        let bodies = []

        bodies.push(Matter.Bodies.circle(100, 200, 50, {render: {
            fillStyle: 'white'
          }}))

        this.symbols.forEach((symbol) => {
            bodies.push(symbol.display())
        })

        return bodies
    }

    addSymbol(params) {
        let symbol = new Symbol(params)

        this.symbols.push(symbol)
    }
}