export default class User {
    constructor(params) {
        this.name = params.name
        this.id = params.id
    }

    display(){
        console.log(`User: ${this.id} => ${this.name}`)
    }
}