export default class Legend {
    constructor(params) {
        this.name = params.name
        this.symbol = params.symbol
    }

    display() {
        console.log(`Legend: ${this.name} => ${this.symbol}`)
    }
}