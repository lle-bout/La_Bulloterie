import Bulloterie from "/src/bulloterie.js"

if (module.hot) {
    module.hot.accept()
}

let data = {
    "users": [
        {
            "name": "Tony",
            "id": "1"

        },
        {
            "name": "Nyto",
            "id": "2"
        },
        {
            "name": "marie",
            "id": "4"
        }
    ],
    "legends": [
        {
            "name": "expert",
            "symbol": "SVGPathSegCurvetoQuadraticRel"
        },
        {
            "name": "passionné",
            "symbol": "SVGEllipseElement"
        },
        {
            "name": "intéressé",
            "symbol": "SVGPoint"
        }
    ],
    "bulles": [
        {
            "name": "Architecture",
            "symbols": [
                {
                    "user": "Tony",
                    "legend": "intéressé"
                }
            ]
        },
        {
            "name": "Salades",
            "symbols": [
                {
                    "user": "Nyto",
                    "legend": "expert"
                },
                {
                    "user": "marie",
                    "legend": "intéressé"
                }
                ,
                {
                    "user": "Tony",
                    "legend": "intéressé"
                }
            ]
        }
    ]
}

var bulloterie = new Bulloterie({ "name": "BubblOne" });

bulloterie.import(data)

bulloterie.display()