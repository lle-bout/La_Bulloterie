import * as Matter from 'matter-js'

export default class Symbol {
    constructor(params) {
        this.user = params.user
        this.legend = params.legend
    }

    display() {
        //console.log(`  Symbol: ${this.legend.name} => ${this.user.name}`)
        return Matter.Bodies.circle(100, 200, 5, {render: {
            fillStyle: 'blue'
          }})
    }
}